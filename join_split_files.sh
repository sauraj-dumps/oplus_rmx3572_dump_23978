#!/bin/bash

cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat my_preload/del-app/snackvideo/com.kwai.bulldog.apk.* 2>/dev/null >> my_preload/del-app/snackvideo/com.kwai.bulldog.apk
rm -f my_preload/del-app/snackvideo/com.kwai.bulldog.apk.* 2>/dev/null
cat my_preload/del-app/Helo/world.social.group.video.share.apk.* 2>/dev/null >> my_preload/del-app/Helo/world.social.group.video.share.apk
rm -f my_preload/del-app/Helo/world.social.group.video.share.apk.* 2>/dev/null
cat my_preload/del-app/lazada/com.lazada.android.apk.* 2>/dev/null >> my_preload/del-app/lazada/com.lazada.android.apk
rm -f my_preload/del-app/lazada/com.lazada.android.apk.* 2>/dev/null
cat my_preload/del-app/shopee_id/com.shopee.id.apk.* 2>/dev/null >> my_preload/del-app/shopee_id/com.shopee.id.apk
rm -f my_preload/del-app/shopee_id/com.shopee.id.apk.* 2>/dev/null
cat my_preload/del-app/tiktok_trill/com.ss.android.ugc.trill.apk.* 2>/dev/null >> my_preload/del-app/tiktok_trill/com.ss.android.ugc.trill.apk
rm -f my_preload/del-app/tiktok_trill/com.ss.android.ugc.trill.apk.* 2>/dev/null
cat my_preload/del-app/spotify_sea/com.spotify.music.apk.* 2>/dev/null >> my_preload/del-app/spotify_sea/com.spotify.music.apk
rm -f my_preload/del-app/spotify_sea/com.spotify.music.apk.* 2>/dev/null
cat my_product/app/OplusCamera/OplusCamera.apk.* 2>/dev/null >> my_product/app/OplusCamera/OplusCamera.apk
rm -f my_product/app/OplusCamera/OplusCamera.apk.* 2>/dev/null
cat system/system/apex/com.google.android.art.apex.* 2>/dev/null >> system/system/apex/com.google.android.art.apex
rm -f system/system/apex/com.google.android.art.apex.* 2>/dev/null
cat my_stock/priv-app/OppoGallery2/OppoGallery2.apk.* 2>/dev/null >> my_stock/priv-app/OppoGallery2/OppoGallery2.apk
rm -f my_stock/priv-app/OppoGallery2/OppoGallery2.apk.* 2>/dev/null
cat my_bigball/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> my_bigball/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f my_bigball/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat my_bigball/app/Photos/Photos.apk.* 2>/dev/null >> my_bigball/app/Photos/Photos.apk
rm -f my_bigball/app/Photos/Photos.apk.* 2>/dev/null
cat my_bigball/app/Browser/Browser.apk.* 2>/dev/null >> my_bigball/app/Browser/Browser.apk
rm -f my_bigball/app/Browser/Browser.apk.* 2>/dev/null
cat my_bigball/priv-app/Mms/Mms.apk.* 2>/dev/null >> my_bigball/priv-app/Mms/Mms.apk
rm -f my_bigball/priv-app/Mms/Mms.apk.* 2>/dev/null
cat my_bigball/priv-app/KeKeUserCenter/KeKeUserCenter.apk.* 2>/dev/null >> my_bigball/priv-app/KeKeUserCenter/KeKeUserCenter.apk
rm -f my_bigball/priv-app/KeKeUserCenter/KeKeUserCenter.apk.* 2>/dev/null
cat odm/lib64/libstblur_capture_api.so.* 2>/dev/null >> odm/lib64/libstblur_capture_api.so
rm -f odm/lib64/libstblur_capture_api.so.* 2>/dev/null
cat system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system_ext/priv-app/SystemUI/SystemUI.apk
rm -f system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat my_heytap/app/Maps/Maps.apk.* 2>/dev/null >> my_heytap/app/Maps/Maps.apk
rm -f my_heytap/app/Maps/Maps.apk.* 2>/dev/null
cat my_heytap/app/YouTube/YouTube.apk.* 2>/dev/null >> my_heytap/app/YouTube/YouTube.apk
rm -f my_heytap/app/YouTube/YouTube.apk.* 2>/dev/null
cat my_heytap/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> my_heytap/app/WebViewGoogle/WebViewGoogle.apk
rm -f my_heytap/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat my_heytap/app/Gmail2/Gmail2.apk.* 2>/dev/null >> my_heytap/app/Gmail2/Gmail2.apk
rm -f my_heytap/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat my_heytap/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> my_heytap/priv-app/GmsCore/GmsCore.apk
rm -f my_heytap/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat my_heytap/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> my_heytap/priv-app/Velvet/Velvet.apk
rm -f my_heytap/priv-app/Velvet/Velvet.apk.* 2>/dev/null
